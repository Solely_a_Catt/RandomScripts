use std::fmt;

/// A Logging enum wrapping messages
pub enum Logging<'a> {
    Trace(&'a str),
    Debug(&'a str),
    Info(&'a str),
    Warn(&'a str),
    Error(&'a str),
    Fatal(&'a str),
}


impl fmt::Display for Logging<'_> {
    /// Actually display the log message
    ///
    /// # Example
    /// ```rust
    ///     println!("{}", Logging::Debug("Entering function xy"));
    /// ```
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Could have saved a line of code here by putting this match case
        // inside write!, but nah
        let log = match self {
            // Green
            Logging::Trace(msg) => format!("{:20} {}",
                "\x1b[1:32m[TRACE]\x1b[0m",  msg
            ),
            // Cyan
            Logging::Debug(msg) => format!("{:20} {}",
                "\x1b[1:36m[DEBUG]\x1b[0m", msg
            ),
            // Blue
            Logging::Info(msg) => format!("{:20} {}",
                "\x1b[1:34m[INFO]\x1b[0m", msg
            ),
            // Yellow
            Logging::Warn(msg) => format!("{:20} {}",
                "\x1b[1:33m[WARNING]\x1b[0m", msg
            ),
            // Red
            Logging::Error(msg) => format!("{:20} {}",
                "\x1b[1:31m[ERROR]\x1b[0m", msg
            ),
            // Magenta
            Logging::Fatal(msg) => format!("{:20} {}",
                "\x1b[1:35m[FATAL]\x1b[0m", msg
            ),
        };

        write!(f, "{}", log)
    }
}
