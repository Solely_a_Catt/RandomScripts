use rust_playground::logging::Logging::{*};


fn main() {
    println!("{}", Trace("Now starting main function"));
    println!("{}", Debug("This line gets executed"));
    println!("{}", Info("Reading that is a waste of time"));
    println!("{}", Warn("Don't ignore this warning -_-"));
    println!("{}", Error("You see, now you have an error"));
    println!("{}", Fatal("Bailing out, you are on your own now :( Good luck"));
}
